<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Laravel\Passport\Client;
use Illuminate\Support\Facades\Route;

class RegisterController extends Controller
{   
	use  IssueTokenTrait; 
	private $client;
	public function __construct(){
		$this->client = Client::find(1);
	}

    public function register(Request $request){
         $this->validate($request, [
          'name' => 'required|max:255',
          'email' => 'required|email|unique:users,email',
          'password'=>'required|min:6'
         ]);

          $user = User::create([
    		'name' => request('name'),
    		'email' => request('email'),
    		'password' => bcrypt(request('password')),
        'verified' => User::UNVERIFIED_USER,
        'authentic' => User::UNAUTHENTIC_USER,
        'admin'   => User::REGULAR_USER
    	]);
        
        return $this->issueToken($request, 'password');



    	 // dd($request-> all());
    }
}
