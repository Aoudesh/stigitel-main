<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Client;


class LoginController extends Controller
{
   use IssueTokenTrait; 
   private $client;

   public function __construct(){
   	$this->client = Client::find(1);
   }

    public function login(Request $request){
    	$this->validate($request,[
         'username' => 'required',
         'password' => 'required'
    	]);

   return $this->issueToken($request, 'password');


    }
    public function refresh(Request $request){
         $this->validate($request,[
         'refresh_token' => 'required',
    	]);

     return $this->issueToken($request, 'refresh_token');

    	
    }
    public function logout(Request $request){

    	$accessToken = Auth::user()->token();
    	 DB::table('oauth_refresh_tokens')
    	   ->where('access_token_id', $accessToken->id)
    	   ->update(['revoked' => true ]);

    	   $accessToken->revoke();
    	   return response()->json([],204);
    	
    }
    // here user details will allowed to show 

    public function details(){

    	$user = Auth::user();
    	return response()->json(['data' => $user], 200,[],JSON_NUMERIC_CHECK);
    }

   public function update(Request $r){
    \APP\User::find(auth()->user()->id)->update(['name'=>$r->name,'email'=>$r->email,'password' => bcrypt(request('password'))]);
    return response()->json(['sucess'=>'sucessfully updated']);
   }

   public function store(Request $request){
     
      $request->file('image');
     //return $request->image->store('public');
      return Storage::putFile('public',$request->file('image'));
   }

}


/*to show 
public function show (){
  $url = Storage::url('$image');
  return "<img src='".$url._"'/>";
}