<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Files;
use DB;
use App\Http\Resources\User as UserResource;
use Illuminate\Support\Facades\Auth;

 
class UserresourceController extends Controller
{
    public function getUser(){

    	return new UserResource(auth()->user());
    } 

    public function getUserall () {
    return UserResource::collection(User::all()->except(Auth::id()));
    }

     public function show($id)
     { 
     	$user = User::find($id);
     	if($user == null){
            return response()->json(['oops'=>'User not found']);
     	}else{
     	return response()->json(['data' => $user], 200,[],JSON_NUMERIC_CHECK);}
     }
      public function action($id, Request $request)
      {

                $user = Auth::user();
                $User =Auth::user()->isVerified();
        switch ($request->get('act')) {
        case "follow":
            $user->following()->attach($id);
              return response()->json(['sucess'=>'sucessfully followed']);
            break;
        case "unfollow":
            $user->following()->detach($id);
              return response()->json(['sucess'=>'sucessfully unfollwed']);
            break;
            default:
              return response()->json(['error'=>'something went wrng']);
              }

 
        }

      // public function partaction($id, Request $request){
      //   $user = Auth::user();
      //   if($user->isVerified()){
      //       $request->has('follow');
      //       $user->following()->attach($id);
      //       DB::table('users')
      //       ->where('id', $id)
      //       ->update(['verified' => 1]);
      //       return response()->json(['sucess'=>'sucessfully verified & follwed']);
      //   }elseif (!$user->isVerified()) {
      //       $request->has('follow');
      //       $user->following()->attach($id);
      //       return response()->json(['sucess'=>'sucessfully follwed']);
      //   }else {
      //       if (!$user->isVerified()) {
      //        $request->has('unfollow');
      //       // $user->following()->detach($id);
      //      $user = DB::table('users')
      //       ->where('id', $id)
      //       ->update(['verified' => 0]);
      //       return response()->json(['sucess'=>'sucessfully unfollwed']);
      //       }
           
      //    } 
      // }  

        public function adminpanel($id, Request $request){
            $user=Auth::user()->isAdmin();
            switch($request->get('action')){
                case "follow":
             $update  = DB::table('users')
            ->where('id', $id)
            ->update(['verified' => 1]);
             return response()->json(['sucess'=>'sucessfully verified']);
             break;
             case "unfollow":
              $update  = DB::table('users')
            ->where('id', $id)
            ->update(['verified' => 0]);
             return response()->json(['sucess'=>'sucessfully unverified']);
             break;
             case "auth":
              $update  = DB::table('users')
            ->where('id', $id)
            ->update(['authentic' => 1]);
             return response()->json(['sucess'=>'sucessfully Authenticated_user']);
             break;
             case "unauth":
              $update  = DB::table('users')
            ->where('id', $id)
            ->update(['authentic' => 0]);
             return response()->json(['sucess'=>'sucessfully unAuthenticated_user']);
             break;
             default:
             return response()->json(['error'=>'something went wrng']);

            }
        }

       //  public function index( User $user){

       // $user =  $users->files();
       //     return response()->json(['data' => $user], 200,[],JSON_NUMERIC_CHECK);

       //  }
        
}

