<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Files;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
class FileController extends Controller{
public function store(Request $request)
    {
        // validation
        $this->validate($request, [
            'title' => 'required',
            'description' => 'nullable',
            'file' => 'nullable|file|mimes:jpeg,jpg,png,gif,mp4','mpeg','ogg','webm','3gp','mov','flv','avi',
            'audio' =>'nullable|file'
        ]); 
                  $file = new Files;
         // code for upload 'file'
           $title = $request->title;
           $description = $request->description;

          if($request->hasFile('file')){
            $uniqueid=uniqid();
            $original_name=$request->file('file')->getClientOriginalName();
            $type=$request->file('file')->getType();
            $fileextension=$request->file('file')->getClientOriginalExtension();
            $name=Carbon::now()->format('Ymd').'_'.$uniqueid.'.'.$fileextension;
            $imagepath=url('/storage/upload/files/'.$name);
            $path=$request->file('file')->storeAs('public/upload/files/',$name);  
            $file->file_path = $imagepath;
           }
        // code for upload 'audio'
        // handle multiple files 
       if(is_array($request->file('audio')))
        {
         $audios=array();
         foreach($request->file('audio') as $file) {
            $uniqueid=uniqid();
            $original_name=$file->getClientOriginalName();
            $size=$file->getSize();
            $extension=$file->getClientOriginalExtension();
            $filename=Carbon::now()->format('Ymd').'_'.$uniqueid.'.'.$extension;
            $audiopath=url('/storage/upload/files/audio/'.$filename);
            $path=$file->storeAs('/upload/files/audio',$filename);
            array_push($audios,$audiopath);
         }
         $all_audios=implode(",",$audios);
     }else{ 
         // handle single file 

         if($request->hasFile('audio')){
         $uniqueid=uniqid();
         $original_name=$request->file('audio')->getClientOriginalName();
         $type=$request->file('audio')->getType();
         $fileextension=$request->file('audio')->getClientOriginalExtension();
         $filename=Carbon::now()->format('Ymd').'_'.$uniqueid.'.'.$fileextension;
         $audiopath=url('/storage/upload/files/audio/'.$filename);
         $path=$request->file('audio')->storeAs('public/upload/files/audio/',$filename);
         $all_audios=$audiopath;
         $file->audio_path = $audiopath;
         $file->extension = $fileextension;
         $file->type = $type;

        }
    }
         //$file = new Files;
         $file->title = $title;
         $file->description = $description;
         // $file->file_path = $imagepath;
         //$file->audio_path = $audiopath;
         $file->user_id = Auth::user()->id;
         $file->save();
         return response()->json(['data' => $file], 200,[],JSON_NUMERIC_CHECK);

 }
 public function index(){
  $files = Files::all();
  return response()->json(['data'=> $files],200,[],JSON_NUMERIC_CHECK);
 }
 public function show($id){
  $files = Files::find($id);
  return response()->json(['data'=> $files],200,[],JSON_NUMERIC_CHECK);
 }
 public function delete($id){
  $files = Files::find($id);
  if (is_null($files)) {
       return response()->json(['Error'=>'Post not found']); 
  }
  $files->delete();
  return response()->json(['sucess'=>'sucessfully Deleted']);
 }
 public function update(Request $request, $id){
        // validation
        $this->validate($request, [
            'title' => 'required',
            'description' => 'nullable',
            'file' => 'nullable|file|mimes:jpeg,jpg,png,gif,mp4','mpeg','ogg','webm','3gp','mov','flv','avi',
            'audio' =>'nullable|file'
        ]); 
                  $file =  Files::find($id);
         // code for upload 'file'
           $title = $request->title;
           $description = $request->description;

          if($request->hasFile('file')){
            $uniqueid=uniqid();
            $original_name=$request->file('file')->getClientOriginalName();
            $type=$request->file('file')->getType();
            $fileextension=$request->file('file')->getClientOriginalExtension();
            $name=Carbon::now()->format('Ymd').'_'.$uniqueid.'.'.$fileextension;
            $imagepath=url('/storage/upload/files/'.$name);
            $path=$request->file('file')->storeAs('public/upload/files/',$name);  
            $file->file_path = $imagepath;
           }
        // code for upload 'audio'
        // handle multiple files 
       if(is_array($request->file('audio')))
        {
         $audios=array();
         foreach($request->file('audio') as $file) {
            $uniqueid=uniqid();
            $original_name=$file->getClientOriginalName();
            $size=$file->getSize();
            $extension=$file->getClientOriginalExtension();
            $filename=Carbon::now()->format('Ymd').'_'.$uniqueid.'.'.$extension;
            $audiopath=url('/storage/upload/files/audio/'.$filename);
            $path=$file->storeAs('/upload/files/audio',$filename);
            array_push($audios,$audiopath);
         }
         $all_audios=implode(",",$audios);
     }else{ 
         // handle single file 

         if($request->hasFile('audio')){
         $uniqueid=uniqid();
         $original_name=$request->file('audio')->getClientOriginalName();
         $type=$request->file('audio')->getType();
         $fileextension=$request->file('audio')->getClientOriginalExtension();
         $filename=Carbon::now()->format('Ymd').'_'.$uniqueid.'.'.$fileextension;
         $audiopath=url('/storage/upload/files/audio/'.$filename);
         $path=$request->file('audio')->storeAs('public/upload/files/audio/',$filename);
         $all_audios=$audiopath;
         $file->audio_path = $audiopath;
         $file->extension = $fileextension;
         $file->type = $type;

        }
    }
         $file->title = $title;
         $file->description = $description;
         $file->user_id = Auth::user()->id;
         $file->save();
         return response()->json(['data' => $file], 200,[],JSON_NUMERIC_CHECK);
 }
}