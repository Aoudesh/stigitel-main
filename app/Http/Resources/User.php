<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'profile_pic' =>$this->profile_pic,
            'admin' =>$this->admin,
            'authentic'  =>$this->authentic,
            'verified'   =>$this->verified,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' =>$this->deleted_at,
    
            'links' => [
                 [
                   'href' => route('users.index', $this->id),
                 ],
             ]


         ];   

    }
}
