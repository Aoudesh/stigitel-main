<?php

namespace App;
use App\User;
use App\Comment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Files extends Model
{
	    use SoftDeletes;
       public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
    public function Comment(){
    return $this->hasMany(Comment::class);
    }
}
