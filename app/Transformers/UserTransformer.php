<?php

namespace App\Transformers;
use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'identifier'  => (int)$user->id,
             'name' => (string)$user->name,
             'email' => (string)$user->email,
             'isVerified' => (int)$user->verified,
             'isAuthentic' =>(int)$user->authentic,
             'isAdmin' =>($user->admin === 'true'),
             'creationDate' => $user->created_at,
             'lastchange' => $user->updated_at,
             'deletedDate' => isset($user->deleted_at) ? (string) $user ->deleted_at :null,
        ];
    }
}
