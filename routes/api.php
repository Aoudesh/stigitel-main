<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//////////////              API               ///////////////////

Route::post('register','Api\Auth\RegisterController@register');
Route::post('login','Api\Auth\LoginController@login');
Route::post('refresh','Api\Auth\LoginController@refresh');




Route::middleware('auth:api')->group(function () {

    Route::post('logout', 'Api\Auth\LoginController@logout');
    Route::get('details','Api\Auth\LoginController@details');
    Route::patch('/user/update','Api\Auth\LoginController@update');
    Route::get('/getUser','Api\UserresourceController@getUser');
    Route::resource('users', 'Api\UserresourceController')->only([
    'index', 'show']);
   // Route::get('/users/show/{id}','Api\UserresourceController@show');
    Route::get('/getUserall','Api\UserresourceController@getUserall');//ye admin klia


    /// file///
    Route::post('store','Api\FileController@store');
    Route::get('/user/{id}','Api\UserresourceController@user');
    Route::post('index','Api\FileController@index');
    Route::get('/show/{id}','Api\FileController@show');
    Route::delete('/delete/{id}','Api\FileController@delete');
    Route::patch('/update/{id}','Api\FileController@update');
    //// file ////


    Route::post('users/{id}/action', 'Api\UserresourceController@action');
      Route::post('users/{id}/adminpanel', 'Api\UserresourceController@adminpanel'); 
      Route::post('users/{id}/partaction','Api\UserresourceController@partaction');
      /// comment //


      Route::post('comment/{id}','Api\CommentController@comment');
      Route::patch('/files/{id}/comment/{commentid}','Api\CommentController@update');
      Route::delete('comment/{commentid}','Api\CommentController@delete');
    //  Route::get('/files/{id}/comment','Api\CommentController@index');

      /// comment ///
      Route::get('/user/{id}/','Api\UserFilesController@index');

});


//////////////        WEB API        ///////////////////////////

    //Route::post('store','Api\Auth\LoginController@store');
    //Route::post('store','Api\FileController@store');
  //  Route::post('show','Api\FileController@show');
//admin panel
//Route::get('/getUserall','Api\UserresourceController@getUserall');